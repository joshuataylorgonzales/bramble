defmodule BrambleWeb.PageLiveTest do
  use BrambleWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Bramble.{
    Accounts.User,
    Repo
  }

  describe "PageLive" do
    test "the page should render", %{conn: conn} do
      {:ok, _view, _html} = live(conn, "/")
    end

    test "the user should persist when the form is submitted", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")

      render_submit(view, :save, %{
        "user" => %{
          full_name: "John Woe",
          quality: 100
        }
      })

      assert Repo.get_by(User, full_name: "John Woe")
    end

    test "the table should be updated when submit succeeds", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")

      view
      |> element(".form-container")
      |> render_submit(%{
        "user" => %{
          full_name: "John Woe",
          quality: 100
        }
      })

      assert view |> element(".data > .name") |> render() =~ "John Woe"
    end

    test "the average should change when the submit succeeds", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")

      assert view |> element("[data-j_r_ave]") |> render() =~ "0"

      view
      |> element(".form-container")
      |> render_submit(%{
        "user" => %{
          full_name: "John Woe",
          quality: 100
        }
      })

      assert view |> element("[data-j_r_ave]") |> render() =~ "100"
    end
  end
end
