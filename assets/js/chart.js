export const pieChart = data => {
   new Chartist.Pie('.ct-chart', {
     labels: ["A-I", "J-R", "S-Z"],
     series: data
   },
   {
    labelOffset: 20,
    height: "300px"
    });
}

 