// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"
import {pieChart} from "./chart.js"

let Hooks = {}
Hooks.ChartComponent = {
  getValues() {
    const a_i = parseInt(this.el.dataset.a_i_ave)
    const j_r = parseInt(this.el.dataset.j_r_ave)
    const s_z = parseInt(this.el.dataset.s_z_ave)
    return [{
      value: a_i,
      className: "aIAverage"
    },
    {
      value: j_r,
      className: "jRAverage"
    }, 
    {
      value: s_z, 
      className: "sZAverage"
    }]
  },
  mounted() {
   return pieChart(this.getValues())
  },
  updated() {
   return pieChart(this.getValues())
  }
  
}

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken}, hooks: Hooks})

// Show progress bar on live navigation and form submits
window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket

