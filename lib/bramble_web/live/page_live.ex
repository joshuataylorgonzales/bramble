defmodule BrambleWeb.PageLive do
  use BrambleWeb, :live_view

  alias Bramble.{
    Accounts,
    Accounts.User
  }

  @a_i for n <- ?a..?i, do: <<n::utf8>>
  @j_r for n <- ?j..?r, do: <<n::utf8>>
  @s_z for n <- ?s..?z, do: <<n::utf8>>

  def mount(_params, _session, socket) do
    if connected?(socket), do: Phoenix.PubSub.subscribe(Bramble.PubSub, "bramble")
    users = Accounts.get_all_users()
    data = build_data(users, %{a_i: [], j_r: [], s_z: []})

    socket =
      socket
      |> assign(a_i_ave: get_ave(Enum.sum(data[:a_i]), Enum.count(data[:a_i])))
      |> assign(j_r_ave: get_ave(Enum.sum(data[:j_r]), Enum.count(data[:j_r])))
      |> assign(s_z_ave: get_ave(Enum.sum(data[:s_z]), Enum.count(data[:s_z])))
      |> assign(users: users)
      |> assign(data: data)
      |> assign(changeset: User.changeset(%{}))

    {:ok, socket}
  end

  def get_ave(sum, length) when sum != 0, do: sum / length
  def get_ave(_sum, _length), do: 0

  defp build_data([], data), do: data

  defp build_data(users, data) do
    Enum.reduce(users, data, fn user, acc ->
      cond do
        name_check(user.full_name, @a_i) ->
          Map.update(acc, :a_i, [user.quality], fn x -> [user.quality | x] end)

        name_check(user.full_name, @j_r) ->
          Map.update(acc, :j_r, [user.quality], fn x -> [user.quality | x] end)

        name_check(user.full_name, @s_z) ->
          Map.update(acc, :s_z, [user.quality], fn x -> [user.quality | x] end)
      end
    end)
  end

  def name_check(name, checker) do
    name
    |> String.downcase()
    |> String.starts_with?(checker)
  end

  def handle_event("save", %{"user" => user}, socket) do
    case Accounts.create_user(user) do
      {:ok, user} ->
        broadcast("bramble", {:user_created, user})
        {:noreply, socket}

      {:error, changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def handle_info({:user_created, user}, %{assigns: %{data: data, users: users}} = socket) do
    data = build_data([user], data)

    socket =
      socket
      |> assign(a_i_ave: get_ave(Enum.sum(data[:a_i]), Enum.count(data[:a_i])))
      |> assign(j_r_ave: get_ave(Enum.sum(data[:j_r]), Enum.count(data[:j_r])))
      |> assign(s_z_ave: get_ave(Enum.sum(data[:s_z]), Enum.count(data[:s_z])))
      |> assign(users: [user | users])
      |> assign(data: data)

    {:noreply, socket}
  end

  defp broadcast(room_name, message) do
    Phoenix.PubSub.broadcast(
      Bramble.PubSub,
      room_name,
      message
    )
  end
end
