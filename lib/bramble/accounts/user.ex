defmodule Bramble.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bramble.Accounts.User

  schema "users" do
    field :full_name, :string
    field :quality, :integer

    timestamps()
  end

  def changeset(params) do
    %User{}
    |> cast(params, [:full_name, :quality])
    |> validate_required([:full_name, :quality])
  end
end
