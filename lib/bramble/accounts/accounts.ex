defmodule Bramble.Accounts do
  import Ecto.Query

  alias Bramble.{
    Accounts.User,
    Repo
  }

  def create_user(params) do
    params
    |> User.changeset()
    |> Repo.insert()
  end

  def get_all_users do
    User
    |> order_by(desc: :inserted_at)
    |> Repo.all()
  end
end
