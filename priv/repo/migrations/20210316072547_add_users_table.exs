defmodule Bramble.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :full_name, :string, null: false
      add :quality, :integer, null: false

      timestamps()
    end
  end
end
