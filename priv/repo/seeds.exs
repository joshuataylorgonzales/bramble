# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bramble.Repo.insert!(%Bramble.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Bramble.Accounts.User
alias Bramble.Repo

Enum.map(1..20, fn _n ->
  Repo.insert!(%User{
    full_name: "#{Faker.Person.first_name()} #{Faker.Person.last_name()}",
    quality: Enum.random(1..100)
  })
end)
