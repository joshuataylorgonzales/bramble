# Bramble Coding Challenge

## Features

- creating a new user with full name and quality ✔
- show a graph of the average quality of user based on the first letter of their name ✔
- real-time graph updates when creating a new user ✔
- optionally added the pub sub updates. you can test it by opening the app simultaneously in multiple browsers and see the "real" real-time happen (●'◡'●)

## Testing the App

- there are two ways to test the app
  - locally:
    - clone the repo then run `docker-compose build` then `docker-compose up`
  - via web:
    - the app is deployed on heroku, you can visit it at: `https://bramble.herokuapp.com`

## Dev Notes

- this challenge is really tricky, since it can be done in multiple ways. i just did what the fastest response time i think i can get.
- also added tests. coz i love tests ༼ つ ◕_◕ ༽つ
